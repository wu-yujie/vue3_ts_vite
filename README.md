# Vue 3 + Typescript + Vite

### 技术栈

- vue3
- naive-ui
- vue-router4
- vueRequest
- pinia

assets----存储全局样式，静态图片资源和共用 icon 图标<br/>
components----存储共用组件<br/>
route----路由<br/>
store----状态管理<br/>
service----封装请求<br/>
pages----页面<br/>

### unocss配置
   - `npm install -D unocss`
    
   ```
    // vite.config.ts
    import UnoCSS from 'unocss/vite'

    export default {
        plugins: [
            UnoCSS({ configFile: '../../uno.config.js', }),
        ],
    }
   ```

   ```
    // src/main.ts
    import 'uno.css'
   ```